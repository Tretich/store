import play.sbt.PlayImport.PlayKeys.devSettings
import sbt.Project.projectToRef
import org.irundaia.sbt.sass._

lazy val uploadsFolder: String =
  (file(".") / "data" / "uploads").getAbsoluteFile.getAbsolutePath

lazy val mysqlDataFolder: String =
  (file(".") / "data" / "mysql").getAbsoluteFile.getAbsolutePath

lazy val mailhogDataFolder: String =
  (file(".") / "data" / "mailhog").getAbsoluteFile.getAbsolutePath

scalaVersion := Settings.versions.scala

lazy val server = (project in file("server"))
  .settings(
    name := Settings.name,
    organization := Settings.organization,
    version := Settings.version,
    scalaVersion := Settings.versions.scala,
    scalacOptions ++= Settings.scalacOptions,
    libraryDependencies ++= Settings.jvmDependencies.value,
    // connect to the client project
    scalaJSProjects := Seq(client),
    pipelineStages in Assets := Seq(scalaJSPipeline),
    pipelineStages := Seq(uglify, digest, gzip),
    compile in Compile := ((compile in Compile) dependsOn scalaJSPipeline).value,

    resolvers ++= Settings.resolvers.value,

    // a simple test support of application
    javaOptions in Test += "-Dconfig.file=conf/application.test.conf",

    // fixes problem with loading entities in prod mode,
    // see: https://github.com/playframework/playframework/issues/4590
    PlayKeys.externalizeResources := false,

    // Fixes: compilation error "File name too long" which can happen on some encrypted or legacy file systems.
    // Please see [SI-3623](https://issues.scala-lang.org/browse/SI-3623) for more details.
    scalacOptions ++= Seq("-Xmax-classfile-name", "100"),

    // Root Path for included CSS in main.less
    SassKeys.assetRootURL := "/",
    // Adjust Urls imported in main.less
    // LessKeys.relativeUrls := true,
    // compress CSS
    // LessKeys.compress in Assets := true,

    maintainer in Docker := "insign gmbh <info@insign.ch>",
    packageName in Docker := "demo",
    version in Docker := System.getProperty("docker.version", Settings.version),
    packageSummary in Docker := "The play-cms demo application.",
    aggregate in Docker := false,
    dockerUsername := Some(System.getProperty("docker.user", "play-cms")),
    dockerRepository := Some(System.getProperty("docker.registry", "docker.insign.rocks")),

    // Add java options for packager
    javaOptions in Universal ++= Seq(
      // -J params will be added as jvm parameters
      "-J-Xmx2g",
      "-J-Xms256m"
    )
  )
  .settings(if (Settings.withDocker) Seq(
    devSettings := {
      val envVars = Map(
        "APPLICATION_HOST"        -> "localhost",
        "MYSQL_HOST"              -> "localhost",
        "MYSQL_PORT"              -> "3306",
        "MYSQL_DATABASE"          -> "db_play-cms-demo",
        "MYSQL_USER"              -> "play-cms-demo",
        "MYSQL_PASSWORD"          -> "s3cr3t",
        "ELASTICSEARCH_ENABLE"    -> "false",
        "ELASTICSEARCH_INDEX"     -> "play-cms-demo",
        "ELASTICSEARCH_HOST"      -> "localhost",
        "ELASTICSEARCH_PORT"      -> "9200",
        "SMTP_MOCK"               -> "false",
        "SMTP_HOST"               -> "localhost",
        "SMTP_PORT"               -> "1025",
        "FILEMANAGER_BASE_URL"    -> "http://localhost:8035/"
      )
      envVars.foreach { case (k, v) =>
        java.lang.System.setProperty(k, v)
      }
      envVars.toSeq
    },
    dockerContainers := Seq(
      /*DockerContainer(
        id = "elasticsearch",
        name = "docker.elastic.co/elasticsearch/elasticsearch",
        version = "6.4.0",
        ports = Seq(
          9200 `:` 9200,
          9300 `:` 9300
        ),
        environment = Map(
          "discovery.type" -> "single-node"
        )
      ),*/
      DockerContainer(
        id = "play-cms-demo_mysql",
        name = "mysql",
        version = "5.7",
        ports = Seq(
          3306 `:` 3306
        ),
        volumes = Seq(
          "/var/lib/mysql" `:` mysqlDataFolder
        ),
        environment = Map(
          "MYSQL_ROOT_PASSWORD" -> "s3cr3t",
          "MYSQL_DATABASE"      -> "db_play-cms-demo",
          "MYSQL_USER"          -> "play-cms-demo",
          "MYSQL_PASSWORD"      -> "s3cr3t"
        )
      ),
      DockerContainer(
        id = "play-cms-demo_mailhog",
        name = "mailhog/mailhog",
        version = "v1.0.0",
        ports = Seq(
          1025 `:` 1025,
          8025 `:` 8025
        ),
        volumes = Seq(
          "/home/mailhog" `:` mailhogDataFolder
        ),
        environment = Map(
          "MH_CORS_ORIGIN"  -> "*",
          "MH_STORAGE"      -> "maildir",
          "MH_MAILDIR_PATH" -> "/home/mailhog"
        )
      ),
      DockerContainer(
        id = "play-cms-demo_filemanager",
        name = "insign/responsive-filemanager",
        ports = Seq(
          80 `:` 8035
        ),
        volumes = Seq(
          "/var/www/html/source" `:` s"$uploadsFolder/source",
          "/var/www/html/thumbs" `:` s"$uploadsFolder/thumbs"
        ),
        environment = Map(
          "AUTH_BASE_URL" -> "http://app:9000", // This works because `app` is in the container's /etc/hosts
          "AUTH_VALIDATE_SSL" -> "false"
        )
      )
    )
  ) else Seq(
    devSettings := {
      val envVars = Map(
        "APPLICATION_HOST"        -> "localhost",
        "MYSQL_DATABASE"          -> "db_play-cms-demo",
        "MYSQL_USER"              -> "play-cms-demo",
        "MYSQL_PASSWORD"          -> "s3cr3t"
      )
      envVars.foreach { case (k, v) =>
        java.lang.System.setProperty(k, v)
      }
      envVars.toSeq
    }
  ))
  .enablePlugins(PlayJava, SbtWeb, DockerPlugin, DockerRun)
  .dependsOn(sharedJvm)
  .devModules(
    "ch.insign" %% "play-cms" % Settings.playCmsVersion as "cms" at "../play-cms" when Settings.playCmsLocal,
    "ch.insign" %% "play-auth" % Settings.playCmsVersion as "auth" at "../play-cms" when Settings.playCmsLocal,
    "ch.insign" %% "play-commons" % Settings.playCmsVersion as "commons" at "../play-cms" when Settings.playCmsLocal,
    "ch.insign" %% "play-theme-metronic" % Settings.playCmsVersion as "metronic" at "../play-cms" when Settings.playCmsLocal)

//////////////////////////
// ScalaJs Integration
//////////////////////////

// This demo-project uses Scala-js in the frontend. Scala-js Projects are usually grouped into three individual projects
// shared, client and server (the actual play-application).
// if you want to write frontend-script in JavaScript, you can remove all code in this Section, but you have to make
// sure, that you also get rid of the following things:
// - remove scala-js-related project folders `shared` and `client`
// - remove scala-js plugin from plugins.sbt
// - remove scala-js relevant settings from server-project (above)
// - get rid of scala-js includes in main.scala.html template and use conventional JavaScript includes instead
// - remove sample implementation of Autowire API as provided in Application, at the following two places: Application.scala::autowireApi and ApiService.scala
// - remove dependencies for unused project in project/Settings.scala

// instantiate the scala-js project
lazy val client: Project = (project in file("client"))
  .settings(
    name := "client",
    version := Settings.version,
    scalaVersion := Settings.versions.scala,
    scalacOptions ++= Settings.scalacOptions,
    libraryDependencies ++= Settings.scalajsDependencies.value,
    // by default we do development build, no eliding
    jsDependencies ++= Settings.jsDependencies.value,
    // RuntimeDOM is needed for tests
    jsEnv := new org.scalajs.jsenv.jsdomnodejs.JSDOMNodeJSEnv(),
    // yes, we want to package JS dependencies
    skip in packageJSDependencies := false,
    // use Scala.js provided launcher code to start the client app
    scalaJSUseMainModuleInitializer := true,
    scalaJSUseMainModuleInitializer in Compile := true,
    scalaJSUseMainModuleInitializer in Test := false,
    // use uTest framework for tests
    testFrameworks += new TestFramework("utest.runner.Framework"),
    resolvers ++= Settings.resolvers.value
  )
  .enablePlugins(ScalaJSPlugin, ScalaJSWeb)
  .dependsOn(sharedJs)

// crossProject for configuring a JS/JVM/shared structure
lazy val shared = (crossProject.crossType(CrossType.Pure) in file("shared"))
  .settings(
    scalaVersion := Settings.versions.scala,
    libraryDependencies ++= Settings.sharedDependencies.value,
    resolvers ++= Settings.resolvers.value
  )
  .jsConfigure(_ enablePlugins ScalaJSWeb)

lazy val sharedJvm = shared.jvm
lazy val sharedJs = shared.js

//////////////////////////
// End of ScalaJs Integration
//////////////////////////

// Set default project
lazy val root = project.in(file("."))
  .settings(
    name := Settings.name,
    onLoad in Global ~= (_ andThen ("project server" :: _))
  )
