resolvers += "Typesafe repository" at "https://repo.typesafe.com/typesafe/maven-releases/"
resolvers += Resolver.bintrayRepo("insign", "play-cms")
resolvers += Resolver.bintrayIvyRepo("insign", "sbt-plugins")

libraryDependencies += "com.typesafe.play" %% "play-json" % "2.6.9"

addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.6.12")
addSbtPlugin("com.typesafe.sbt" % "sbt-play-enhancer" % "1.2.2")
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.3.15")
addSbtPlugin("com.typesafe.sbt" % "sbt-gzip" % "1.0.2")
addSbtPlugin("com.typesafe.sbt" % "sbt-digest" % "1.1.4")
addSbtPlugin("com.typesafe.sbt" % "sbt-uglify" % "2.0.0")
addSbtPlugin("org.irundaia.sbt" % "sbt-sassify" % "1.4.11")
addSbtPlugin("org.scala-js" % "sbt-scalajs" % "0.6.22")
addSbtPlugin("com.vmunier" % "sbt-web-scalajs" % "1.0.7")
addSbtPlugin("ch.insign" %% "sbt-dev-module" % "1.0.4")
addSbtPlugin("com.github.gseitz" % "sbt-release" % "1.0.5")
addSbtPlugin("com.typesafe.sbt" % "sbt-git" % "0.9.3")
