package blocks.pageblock;

import blocks.pageblock.html.edit;
import blocks.pageblock.html.defaultPageBlockSite1;
import blocks.pageblock.html.defaultPageBlockSite2;
import ch.insign.cms.CMSApi;
import ch.insign.cms.models.BlockCache;
import ch.insign.cms.models.PageBlock;
import ch.insign.cms.views.admin.utils.BackUrl;
import play.twirl.api.Html;
import play.data.Form;

import javax.inject.Inject;
import javax.persistence.*;

@Entity
@Table(name = "default_page")
@DiscriminatorValue("DefaultPage")
public class DefaultPage extends PageBlock {

    public static final String SITE_1_KEY = "site1";
	public static final String SITE_2_KEY = "site2";

	@Inject
    private static CMSApi cmsApi;

    @Override
    public Html render() {
		switch (cmsApi.getSites().current().key) {
			case SITE_1_KEY:
				return defaultPageBlockSite1.render(this);

			case SITE_2_KEY:
				return defaultPageBlockSite2.render(this);

			default:
				throw new RuntimeException("Invalid site: " + cmsApi.getSites().current().name);
		}
    }

    public Html editForm(Form editForm) {
        return edit.render(this, editForm, BackUrl.getPrevious(), null);
    }

	/**
	 * Example of using a custom BlockCache configuration
	 */
	@Override
	public BlockCache cache() {
		if (cache == null) {
			cache = new BlockCache(this) {
				/**
				 * Determines if caching should be used (according to the cache configuration and request)
				 * (use isCached() to check if a cached version is available)
				 */
				@Override
				public boolean useCache() {

					// In this example, do not cache the (entire) homepage
					if (PageBlock.KEY_HOMEPAGE.equals(block.getKey())) {
						return false;
					} else {
						return super.useCache();
					}
				}
			};
		}
		return cache;
	}
}
