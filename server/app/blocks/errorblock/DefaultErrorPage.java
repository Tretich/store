package blocks.errorblock;

import ch.insign.cms.blocks.errorblock.ErrorPage;
import ch.insign.cms.CMSApi;
import play.twirl.api.Html;

import javax.inject.Inject;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "default_error_page")
@DiscriminatorValue("DefaultErrorPage")
public class DefaultErrorPage extends ErrorPage {

    public static final String COLLECTION_SLOT = "slot1";

    @Inject
    private static CMSApi cmsApi;

    @Override
    public Html render() {
        String content = cmsApi.getFilterManager().processOutput(
                blocks.errorblock.html.errorShow.render(this).toString(), null);
        return Html.apply(content);
    }
}
