package party.view;

import ch.insign.cms.models.party.view.PartyMenuItemsView;
import ch.insign.cms.models.party.view.support.DefaultPartyProfileView;
import com.google.inject.Inject;
import play.twirl.api.Html;

public class DemoPartyProfileView extends DefaultPartyProfileView {

    @Inject
    public DemoPartyProfileView(PartyMenuItemsView partyMenuItemsView) {
        super(partyMenuItemsView);
    }

    @Override
    public Html render() {
        return views.html.admin.user.profile.render(
                party,
                partyMenuItemsView.setParty(party).render());
    }

}
