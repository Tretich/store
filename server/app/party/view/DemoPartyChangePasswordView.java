package party.view;

import ch.insign.cms.models.party.view.PartyMenuItemsView;
import ch.insign.cms.models.party.view.support.DefaultPartyChangePasswordView;
import ch.insign.cms.views.admin.utils.AdminContext;
import com.google.inject.Inject;
import play.twirl.api.Html;

public class DemoPartyChangePasswordView extends DefaultPartyChangePasswordView {

    @Inject
    public DemoPartyChangePasswordView(PartyMenuItemsView partyMenuItemsView) {
        super(partyMenuItemsView);
    }

    @Override
    public Html render() {
        return views.html.admin.user.passwordForm.render(
                new AdminContext(),
                party,
                passwordForm,
                partyMenuItemsView.setParty(party).render());
    }

}

