package party.view;

import ch.insign.cms.models.party.view.support.DefaultPartyListView;
import play.twirl.api.Html;

public class DemoPartyListView extends DefaultPartyListView {

    @Override
    public Html render() {
        return views.html.admin.user.list.render();
    }
}
