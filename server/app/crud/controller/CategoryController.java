package crud.controller;


import ch.insign.cms.controllers.GlobalActionWrapper;
import ch.insign.cms.permissions.aop.RequiresBackendAccess;
import ch.insign.cms.utils.AjaxResult;
import ch.insign.cms.utils.Error;
import ch.insign.cms.views.admin.utils.AdminContext;
import ch.insign.commons.db.SecureForm;
import ch.insign.playauth.PlayAuthApi;
import crud.data.entity.categories.Category;
import crud.data.form.CategoryForm;
import crud.data.mapper.CategoryMapper;
import crud.data.repository.CategoryRepository;
import crud.permission.CategoryPermission;
import io.vavr.control.Option;
import play.data.Form;
import play.data.FormFactory;
import play.db.jpa.Transactional;
import play.i18n.MessagesApi;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import service.CategoryDatatableService;

import javax.inject.Inject;

@With({GlobalActionWrapper.class})
@RequiresBackendAccess
public class CategoryController extends Controller {

    private final CategoryDatatableService categoryDatatableService;
    private final FormFactory formFactory;
    private final CategoryRepository categoryRepository;
    private final CategoryMapper categoryMapper;
    private final MessagesApi messagesApi;
    private final PlayAuthApi playAuthApi;

    @Inject
    public CategoryController(CategoryDatatableService categoryDatatableService, FormFactory formFactory,
                              CategoryRepository categoryRepository, CategoryMapper categoryMapper,
                              MessagesApi messagesApi, PlayAuthApi playAuthApi) {
        this.categoryDatatableService = categoryDatatableService;
        this.formFactory = formFactory;
        this.categoryRepository = categoryRepository;
        this.categoryMapper = categoryMapper;
        this.messagesApi = messagesApi;
        this.playAuthApi = playAuthApi;
    }

    @Transactional
    public Result list() {
        return ok(crud.views.html.category_list.render());
    }

    @Transactional
    public Result add() {
        Form<CategoryForm> form = formFactory.form(CategoryForm.class);

        return ok(SecureForm.signForms(crud.views.html.category_edit.render(form)));
    }

    @Transactional
    public Result doAdd() {
        Form<CategoryForm> form = formFactory.form(CategoryForm.class).bindFromRequest();

        if (!form.hasErrors()) {
            playAuthApi.requirePermission(CategoryPermission.ADD);
            categoryRepository.save(categoryMapper.fromForm(form.get()));
            flash(AdminContext.MESSAGE_SUCCESS, messagesApi.get(lang(), "crud.category.created"));
            return redirect(routes.CategoryController.list());
        } else {
            return badRequest(crud.views.html.category_edit.render(form));
        }
    }

    @Transactional
    public Result edit(Long id) {
        Option<Category> category = categoryRepository.findById(id);

        if (!category.isEmpty()) {
            playAuthApi.requirePermission(CategoryPermission.EDIT, category.get());
            Form<CategoryForm> form = formFactory.form(CategoryForm.class)
                    .fill(categoryMapper.toForm(category.get()));

            return ok(SecureForm.signForms(crud.views.html.category_edit.render(form)));
        } else {
            return Error.notFound(messagesApi.get(lang(), "crud.category.error.notfound"));
        }
    }
    @Transactional
    public Result doEdit(Long id) {
        Option<Category> category = categoryRepository.findById(id);
        Form<CategoryForm> form = formFactory.form(CategoryForm.class).bindFromRequest();

        if (!form.hasErrors()) {
            if (!category.isEmpty()) {
                playAuthApi.requirePermission(CategoryPermission.EDIT, category.get());
                categoryRepository.save(categoryMapper.update(form.get(), category.get()));
                flash(AdminContext.MESSAGE_SUCCESS, messagesApi.get(lang(), "crud.category.edit.sucsess"));
                return redirect(routes.CategoryController.list());
            } else {
                flash(AdminContext.MESSAGE_ERROR, messagesApi.get(lang(), "crud.category.error.notfound"));
                return redirect(routes.CategoryController.edit(id));
            }
        }else{
            return badRequest(crud.views.html.category_edit.render(form));
        }
    }

    @RequiresBackendAccess
    @Transactional
    public Result deleteRequest(Long id){
        Option<Category> category = categoryRepository.findById(id);

        if (!category.isEmpty()){
            playAuthApi.requirePermission(CategoryPermission.DELETE, category.get());
            return ok(crud.views.html.removeCategoryDialog.render(category.get()));
        }else{
            flash(AdminContext.MESSAGE_ERROR, messagesApi.get(lang(), "crud.category.error.notfound"));
            return redirect(routes.CategoryController.list());
        }
    }

    @Transactional
    public Result delete(Long id) {
        Option<Category> category = categoryRepository.findById(id);

        if (!category.isEmpty()){
            playAuthApi.requirePermission(CategoryPermission.DELETE, category.get());
            categoryRepository.delete(category.get());
            return redirect(routes.CarController.list());
        }else{
         flash(AdminContext.MESSAGE_ERROR, messagesApi.get(lang(), "crud.category.error.notfound"));
         return redirect(routes.CarController.list());
        }
    }

    @Transactional(readOnly = true)
    public Result datatable() {
        return ok(categoryDatatableService.getDatatable(request()));
    }
}
