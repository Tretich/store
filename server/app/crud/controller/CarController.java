package crud.controller;

import ch.insign.cms.controllers.GlobalActionWrapper;
import ch.insign.cms.permissions.aop.RequiresBackendAccess;
import ch.insign.cms.utils.AjaxResult;
import ch.insign.cms.utils.Error;
import ch.insign.cms.views.admin.utils.AdminContext;
import ch.insign.commons.db.SecureForm;
import ch.insign.playauth.PlayAuthApi;
import crud.data.entity.Car;
import crud.data.form.CarForm;
import crud.data.mapper.CarMapper;
import crud.data.repository.BrandRepository;
import crud.data.repository.CarRepository;
import crud.permission.CarPermission;
import crud.util.Paginate;
import crud.views.html.edit;
import crud.views.html.list;
import crud.views.html.removeCarDialog;
import io.vavr.Tuple;
import io.vavr.control.Option;
import org.apache.commons.lang3.StringUtils;
import play.data.Form;
import play.data.FormFactory;
import play.db.jpa.Transactional;
import play.i18n.MessagesApi;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import service.CarDatatableService;

import javax.inject.Inject;

import static io.vavr.API.*;
import static io.vavr.Patterns.$None;
import static io.vavr.Patterns.$Some;

/**
 * Encapsulates a business logic tied to cars management
 */
@With({GlobalActionWrapper.class})
@RequiresBackendAccess
public class CarController extends Controller {

    private final PlayAuthApi playAuthApi;
    private final FormFactory formFactory;
    private final MessagesApi messagesApi;
    private final CarRepository carRepository;
    private final CarMapper carMapper;
    private final BrandRepository brandRepository;
    private final CarDatatableService carDatatableService;

    @Inject
    public CarController(
            PlayAuthApi playAuthApi,
            FormFactory formFactory,
            MessagesApi messagesApi,
            CarRepository carRepository,
            CarMapper carMapper,
            BrandRepository brandRepository,
            CarDatatableService carDatatableService) {
        this.playAuthApi = playAuthApi;
        this.formFactory = formFactory;
        this.messagesApi = messagesApi;
        this.carRepository = carRepository;
        this.carMapper = carMapper;
        this.brandRepository = brandRepository;
        this.carDatatableService = carDatatableService;
    }

    /**
     * Initializes car form includes validation rules
     * @return edit template with secure form to render
     */
    @Transactional
    public Result add() {
        /* Prevent unauthorized users to access this page.
         * @see {https://confluence.insign.ch/display/PLAY/Play+Auth+-+Authorization#PlayAuth-Authorization-ProtectingControllers}
         */
        playAuthApi.requirePermission(CarPermission.ADD);

        Form<CarForm> form = formFactory.form(CarForm.class);

        /* Protect the form from parameter tampering attacks.
         * @see {https://confluence.insign.ch/display/PLAY/Play+Commons#PlayCommons-SecureForm}
         */
        return ok(SecureForm.signForms(edit.render(form, brandRepository.findAll().getAll().asJava())));
    }

    /**
     * Binds data from request, validate its data and if hasn't errors, persist its data to database
     * @return redirect to car list page if new car was saved or bad request, if something was wrong
     */
    @Transactional
    public Result doAdd() {
        playAuthApi.requirePermission(CarPermission.ADD);

        Form<CarForm> form = formFactory.form(CarForm.class).bindFromRequest();

        return Match(form.hasErrors()).of(
                        Case($(true), () -> badRequest(SecureForm.signForms(edit.render(form, brandRepository.findAll().getAll().asJava())))),
                        Case($(false), () -> carRepository.save(carMapper.fromForm(form.get()))
                                .map(c -> {
                                    flash(AdminContext.MESSAGE_SUCCESS, messagesApi.get(lang(), "example.crud.car.add.successful.message", c.getModel()));
                                    return redirect(routes.CarController.list());
                                })
                                .getOrElse(Error.internal(messagesApi.get(lang(), "example.crud.car.add.failed.message"))))
        );
    }

    /**
     * Finds a car by specified id and if exists, initialize a filled form includes validation rules
     * @param id - car id
     * @return edit template with secure form to render or not found page with error message
     */
    @Transactional
    public Result edit(Long id) {
        return carRepository.findOneById(id).map(car -> {
            playAuthApi.requirePermission(CarPermission.EDIT, car);

            Form<CarForm> form = formFactory.form(CarForm.class).fill(carMapper.toForm(car));

            return ok(SecureForm.signForms(edit.render(form, brandRepository.findAll().getAll().asJava())));
        }).getOrElse(Error.notFound(messagesApi.get(lang(), "example.crud.car.error.notfound", id)));
    }

    /**
     * Binds data from request, validate its data and saves its if hasn't errors
     * @param id - car id
     * @return redirect to car list page if success or throws bad request is errors present
     */
    @Transactional
    public Result doEdit(Long id) {
        return carRepository.findOneById(id)
                .peek(car -> playAuthApi.requirePermission(CarPermission.EDIT, car))
                .map(car -> Tuple.of(car, formFactory.form(CarForm.class).bindFromRequest()))
                .map(t -> Match(t._2.hasErrors()).of(
                        Case($(true), () -> badRequest(SecureForm.signForms(edit.render(t._2, brandRepository.findAll().getAll().asJava())))),
                        Case($(false), () -> carRepository.save(carMapper.update(t._2.get(), t._1))
                                .map(c -> {
                                    flash(AdminContext.MESSAGE_SUCCESS, messagesApi.get(lang(), "example.crud.car.edit.successful.message", c.getModel()));
                                    return redirect(routes.CarController.list());
                                })
                                .getOrElse(Error.internal(messagesApi.get(lang(), "example.crud.car.edit.failed.message"))))
                ))
                .getOrElse(Error.notFound(messagesApi.get(lang(), "example.crud.car.notfound.message", id)));
    }

    /**
     * Delete a car by specified id
     * @param id - car id
     * @return redirect to car list page if success or not found status, if no car present with specified id
     */
    @Transactional
    public Result delete(Long id) {
        return carRepository.findOneById(id)
                .peek(car -> playAuthApi.requirePermission(CarPermission.DELETE, car))
                .map(car -> carRepository.delete(car)
                        .map(v -> AjaxResult.ok(messagesApi.get(lang(), "example.crud.car.delete.successful.message")))
                        .getOrElse(Error.internal(messagesApi.get(lang(), "example.crud.car.error.delete"))))
                .getOrElse(Error.notFound(messagesApi.get(lang(), "example.crud.car.error.notfound", id)));
    }

    /**
     * Prints a list with all cars with possibility to filter its data by brand/pagination
     * @return a page - the list of cars with pagination to render
     */
    @Transactional(readOnly = true)
    public Result list() {
        return ok(list.render());
    }

    @Transactional(readOnly =  true)
    public Result datatable() {
        return ok(carDatatableService.getDatatable(request()));
    }

    private Paginate<Car> findWithSearchTerm(String searchTerm) {
        Paginate<Car> searchResult = Match(Option.of(searchTerm).filter(s -> StringUtils.isNotBlank(s))).of(
                Case($Some($()), carRepository.findBySearchTerm(searchTerm)),
                Case($None(), carRepository.findAll())
        );
        return searchResult;
    }

    private Paginate<Car> findWithBrandIdAndSearchTerm(Long brandId, String searchTerm) {
        Paginate<Car> searchResult = Match(Option.of(searchTerm).filter(s -> StringUtils.isNotBlank(s))).of(
                Case($Some($()), carRepository.findByBrandIdAndSearchTerm(brandId, searchTerm)),
                Case($None(), carRepository.findByBrandId(brandId))
        );
        return searchResult;
    }

    @RequiresBackendAccess
    @Transactional
    public Result requestDeleteCar(Long id) {
        return carRepository.findOneById(id)
                .peek(car -> playAuthApi.requirePermission(CarPermission.DELETE, car))
                .map(car -> ok(removeCarDialog.render(car)))
                .getOrElse(AjaxResult.error(messagesApi.get(lang(), "example.crud.car.notfound.message", id))) ;
    }
}
