package crud.controller;

import ch.insign.cms.controllers.GlobalActionWrapper;
import ch.insign.cms.permissions.aop.RequiresBackendAccess;
import ch.insign.cms.utils.AjaxResult;
import ch.insign.cms.utils.Error;
import ch.insign.commons.db.SecureForm;
import ch.insign.commons.db.SmartFormFactory;
import ch.insign.playauth.PlayAuthApi;
import crud.data.entity.Product;
import crud.data.form.ProductForm;
import crud.data.mapper.ProductMapper;
import crud.data.repository.CategoryRepository;
import crud.data.repository.ProductRepository;
import crud.permission.ProductPermission;
import io.vavr.control.Option;
import play.data.Form;
import play.data.FormFactory;
import play.db.jpa.Transactional;
import play.i18n.MessagesApi;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import service.ProductDatableService;

import javax.inject.Inject;



@With({GlobalActionWrapper.class})
@RequiresBackendAccess
public class ProductController extends Controller {

    private ProductDatableService productDatableService;
    private final ProductRepository productRepository;
    private final FormFactory formFactory;
    private final MessagesApi messagesApi;
    private final ProductMapper productMapper;
    private final CategoryRepository categoryRepository;
    private final PlayAuthApi playAuthApi;
    private final SmartFormFactory smartFormFactory;

    @Inject
    public ProductController(ProductDatableService productDatableService, ProductRepository productRepository,
                             FormFactory formFactory, MessagesApi messagesApi, ProductMapper productMapper,
                             CategoryRepository categoryRepository, PlayAuthApi playAuthApi,
                             SmartFormFactory smartFormFactory) {
        this.productDatableService = productDatableService;
        this.productRepository = productRepository;
        this.formFactory = formFactory;
        this.messagesApi = messagesApi;
        this.productMapper = productMapper;
        this.categoryRepository = categoryRepository;
        this.playAuthApi = playAuthApi;
        this.smartFormFactory = smartFormFactory;
    }

    @Transactional
    public Result edit(Long id) {
        Option<Product> product = productRepository.findById(id);

        if (!product.isEmpty()) {
            Form<ProductForm> form = smartFormFactory.form(ProductForm.class)
                    .fill(productMapper.toForm(product.get()));

            return ok(SecureForm.signForms(crud.views.html.product_edit.render(form, categoryRepository.findAll().getAll().asJava())));
        } else {
            return Error.notFound(messagesApi.get(lang(), "crud.product.error.notfound", id));
        }
    }


    @Transactional
    public Result doEdit(Long id) {
        Option<Product> prodFromBase = productRepository.findById(id);
        Form<ProductForm> form = smartFormFactory.form(ProductForm.class).bindFromRequest();

        if (!form.hasErrors()) {
            if (!prodFromBase.isEmpty()) {
                productRepository.save(productMapper.update(form.get(), prodFromBase.get()));
                return redirect(routes.ProductController.list());
            } else {
                return AjaxResult.error(messagesApi.get(lang(), "crud.product.notfound.message", id));
            }
        } else {
            return badRequest(SecureForm.signForms(
                    crud.views.html.product_edit.render(
                            form, categoryRepository.findAll().getAll().asJava())));
        }
    }

    @Transactional
    public Result add() {
        Form<ProductForm> form = smartFormFactory.form(ProductForm.class)
                .fill(new ProductForm());

        return ok(SecureForm.signForms(
                crud.views.html.product_edit.render(
                        form, categoryRepository.findAll().getAll().asJava()
                )));
    }

    @Transactional
    public Result doAdd() {
        playAuthApi.requirePermission(ProductPermission.ADD);

        Form<ProductForm> form = smartFormFactory.form(ProductForm.class).bindFromRequest();

        if (!form.hasErrors()) {
            productRepository.save(productMapper.fromForm(form.get()));
            return redirect(routes.ProductController.list());
        } else {
            return badRequest(crud.views.html.product_edit.render(form, categoryRepository.findAll().getAll().asJava()));
        }
    }

    @RequiresBackendAccess
    @Transactional
    public Result deleteRequest(Long id) {
        Option<Product> optionProduct = productRepository.findById(id);

        if (!optionProduct.isEmpty()) {
            Product productForDel = optionProduct.get();
            playAuthApi.requirePermission(ProductPermission.DELETE, productForDel);
            return ok(crud.views.html.removeProductDialog.render(productForDel));
        } else {
            return AjaxResult.error(messagesApi.get(lang(), "crud.product.notfound.message", id));
        }
    }

    @Transactional
    public Result delete(Long id) {

        Product productForDel = productRepository.findById(id).get();
        if (productForDel == null) {
            return Error.internal("crud.product.error.notfound");
        }
        playAuthApi.requirePermission(ProductPermission.DELETE, productForDel);

        boolean result = productRepository.remove(productForDel);

        if (result) {
            return AjaxResult.ok(messagesApi.get(lang(), "crud.product.delete.successful.message"));
        } else {
            return Error.internal("crud.product.error.delete");
        }
    }

    @Transactional(readOnly = true)
    public Result list() {
        return ok(crud.views.html.product_list.render());
    }


    @Transactional(readOnly = true)
    public Result datatable() {
        return ok(productDatableService.getDatatable(request()));
    }
}
