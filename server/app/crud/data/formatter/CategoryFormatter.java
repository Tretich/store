package crud.data.formatter;

import ch.insign.cms.formatters.Formatters;
import crud.data.entity.categories.Category;
import crud.data.repository.CategoryRepository;
import io.vavr.control.Option;

import javax.inject.Inject;
import java.text.ParseException;
import java.util.Locale;

public class CategoryFormatter extends Formatters.SimpleFormatter<Category> {

    private CategoryRepository categoryRepository;

    @Inject
    public CategoryFormatter(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public Category parse(String text, Locale locale) throws ParseException {
        Option<Category> category = categoryRepository.findById(Long.parseLong(text));

        if (!category.isEmpty()){
            return category.get();
        }else {
            throw new ParseException("Category parse error", 0);
        }
    }

    @Override
    public String print(Category category, Locale locale) {
        return String.valueOf(category.getId());
    }
}
