package crud.data.entity;

import ch.insign.commons.db.MString;
import crud.data.entity.categories.Category;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "Product")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "price")
    @Constraints.Min(value = 0, message = "crud.product.price.error.message")
    private BigDecimal price;

    @Column(name = "description")
    private String description;

    @ManyToOne
    private Category category;

    @OneToOne (cascade=CascadeType.ALL)
    private MString image = new MString();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public MString getImage() {
        return image;
    }

    public void setImage(MString image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", description='" + description + '\'' +
                '}';
    }
}
