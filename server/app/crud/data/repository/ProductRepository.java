package crud.data.repository;


import crud.data.entity.Product;
import io.vavr.control.Either;
import io.vavr.control.Option;
import io.vavr.control.Try;
import play.db.jpa.JPAApi;
import play.db.jpa.Transactional;

import javax.inject.Inject;

public class ProductRepository {

    private JPAApi jpaApi;

    @Inject
    public ProductRepository(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    @Transactional
    public Option<Product> findById(Long id){
        return Try.of(() -> Option.of(jpaApi.em().find(Product.class, id))).getOrElse(Option.none());
    }

    public Either<Throwable, Product> save(Product product){
        return Try.of(() -> jpaApi.em().merge(product)).toEither();
    }

    public boolean remove(Product product){
        try{
            jpaApi.em().remove(product);
            return true;
        }catch (Exception ignore){
            return false;
        }
    }
}
