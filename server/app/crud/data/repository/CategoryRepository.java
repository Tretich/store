package crud.data.repository;

import com.uaihebert.factory.EasyCriteriaFactory;
import crud.data.entity.Brand;
import crud.data.entity.categories.Category;
import crud.util.Paginate;
import io.vavr.control.Either;
import io.vavr.control.Option;
import io.vavr.control.Try;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.persistence.EntityManager;

public class CategoryRepository {

    private JPAApi jpaApi;

    @Inject
    public CategoryRepository(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    public Either<Throwable, Category> save(Category category) {
        return Try.of(() -> jpaApi.em().merge(category)).toEither();
    }

    public Try<Void> delete(Category category) {
        return Try.run(() -> {
            EntityManager em  = jpaApi.em();
            em.createQuery("delete from Product p where p.category.id=" + category.getId()).executeUpdate();
            em.remove(category);});
    }

    public Option<Category> findById(Long id) {
        return Try.of(() -> Option.of(jpaApi.em().find(Category.class, id))).getOrElse(Option.none());
    }

    public Paginate<Category> findAll() {
        return Paginate.of(EasyCriteriaFactory.createQueryCriteria(jpaApi.em(), Category.class));
    }
}
