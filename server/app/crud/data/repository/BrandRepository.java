package crud.data.repository;

import com.uaihebert.factory.EasyCriteriaFactory;
import crud.data.entity.Brand;
import crud.util.Paginate;
import io.vavr.collection.Stream;
import io.vavr.control.Either;
import io.vavr.control.Option;
import io.vavr.control.Try;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.persistence.criteria.CriteriaQuery;

public class BrandRepository {

    private final JPAApi jpaApi;

    @Inject
    public BrandRepository(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    public Either<Throwable, Brand> save(Brand brand) {
        return Try.of(() -> jpaApi.em().merge(brand)).toEither();
    }

    public Try<Void> delete(Brand brand) {
        return Try.run(() -> jpaApi.em().remove(brand));
    }

    public Option<Brand> findOneById(long id) {
        return Try.of(() -> Option.of(jpaApi.em().find(Brand.class, id))).getOrElse(Option.none());
    }

    public Paginate<Brand> findAll() {
        return Paginate.of(EasyCriteriaFactory.createQueryCriteria(jpaApi.em(), Brand.class));
    }

}
