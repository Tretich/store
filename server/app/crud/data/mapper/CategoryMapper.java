package crud.data.mapper;

import crud.data.entity.categories.Category;
import crud.data.form.CategoryForm;
import data.mapper.DefaultMapperConfig;
import data.mapper.SimpleMapper;
import org.mapstruct.Mapper;

@Mapper(config = DefaultMapperConfig.class)
public interface CategoryMapper extends SimpleMapper<Category, CategoryForm> {
}

