package crud.data.mapper;

import crud.data.entity.Product;
import crud.data.form.ProductForm;
import data.mapper.DefaultMapperConfig;
import data.mapper.SimpleMapper;
import org.mapstruct.Mapper;

@Mapper(config = DefaultMapperConfig.class)
public interface ProductMapper extends SimpleMapper<Product, ProductForm> {
    default ProductForm toForm(Product e) {
        if (e == null) {
            return null;
        } else {
            ProductForm productForm = new ProductForm();
            productForm.setId(e.getId());
            productForm.setName(e.getName());
            productForm.setDescription(e.getDescription());
            productForm.setPrice(e.getPrice());
            productForm.setCategory(e.getCategory());
            productForm.setImage(e.getImage());
            productForm.setImageId(e.getImage().getId());
            return productForm;
        }
    }

    default Product fromForm(ProductForm f) {
        if (f == null) {
            return null;
        } else {
            Product product = new Product();
            product.setId(f.getId());
            product.setName(f.getName());
            product.setPrice(f.getPrice());
            product.setDescription(f.getDescription());
            product.setCategory(f.getCategory());
            product.setImage(f.getImage());
            product.getImage().setId(f.getImageId());
            return product;
        }
    }

    default Product update(ProductForm source, Product target) {
        if (source == null) {
            return null;
        } else {
            target.setId(source.getId());
            target.setName(source.getName());
            target.setPrice(source.getPrice());
            target.setDescription(source.getDescription());
            target.setCategory(source.getCategory());
            target.setImage(source.getImage());
            target.getImage().setId(source.getImageId());
            return target;
        }
    }
}
