package crud.data.form;

import play.data.validation.Constraints;

public class CategoryForm {

    private Long id;

    @Constraints.Required
    @Constraints.MaxLength(value = 50)
    String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
