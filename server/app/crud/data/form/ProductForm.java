package crud.data.form;

import ch.insign.commons.db.MString;
import crud.data.entity.categories.Category;
import play.data.validation.Constraints;

import javax.validation.constraints.Digits;
import java.math.BigDecimal;

public class ProductForm  {

    private Long id;

    @Constraints.Required
    @Constraints.MaxLength(value = 255)
    private String name;

    @Constraints.Required
    private String description;

    @Constraints.Required
    @Constraints.Min(value = 0, message = "crud.product.price.error.message")
    @Digits(integer = 10, fraction = 2, message = "crud.product.price.error.message")
    private BigDecimal price;

    @Constraints.Required
    private Category category;

    @Constraints.Required
    private MString image;

    private String imageId;

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public MString getImage() {
        return image;
    }

    public void setImage(MString image) {
        this.image = image;
    }
}
