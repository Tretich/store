package crud.page.views;

import ch.insign.cms.models.HtmlView;
import crud.page.CarInventoryPage;
import crud.data.repository.CarRepository;

public interface CarInventoryPageView extends HtmlView {
    CarInventoryPageView setPage(CarInventoryPage page);
    CarInventoryPageView setRepository(CarRepository repository);
}
