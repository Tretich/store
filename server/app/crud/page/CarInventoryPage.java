package crud.page;

import ch.insign.cms.models.PageBlock;
import crud.page.views.CarInventoryPageView;
import crud.data.repository.CarRepository;
import play.data.Form;
import play.mvc.Controller;
import play.twirl.api.Html;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Block class that extends PageBlock archetype to represent car page and enhance its with navigation
 * {@link http://play-cms.com/doc/cms/create-your-own-cms-block}
 */
@Entity
@Table(name = "car_inventory_page")
@DiscriminatorValue("CarInventoryPage")
public class CarInventoryPage extends PageBlock {

    private String dealerTitle;

    @Inject
    private static CarRepository carRepository;

    @Inject
    private static Provider<CarInventoryPageView> viewProvider;

    @Override
    public Html render() {
        return viewProvider.get()
                .setPage(this)
                .setRepository(carRepository)
                .render();
    }

    public Html editForm(Form editForm) {
        String backURL = Controller.request().getQueryString("backURL");
        return crud.page.views.html.edit.render(this, editForm, backURL, null);
    }

    public String getDealerTitle() {
        return dealerTitle;
    }

    public void setDealerTitle(String dealerTitle) {
        this.dealerTitle = dealerTitle;
    }
}
