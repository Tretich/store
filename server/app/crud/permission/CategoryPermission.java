package crud.permission;

import ch.insign.playauth.authz.DomainPermissionEnum;
import crud.data.entity.categories.Category;

public enum CategoryPermission implements DomainPermissionEnum<Category> {
    EDIT, ADD, DELETE, BROWSE;
}
