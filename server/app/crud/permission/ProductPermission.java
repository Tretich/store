package crud.permission;

import ch.insign.playauth.authz.DomainPermission;
import ch.insign.playauth.authz.DomainPermissionEnum;
import crud.data.entity.Product;

public enum ProductPermission implements DomainPermissionEnum<Product> {
    EDIT, ADD, DELETE, BROWSE
}
