package data.validator;

import ch.insign.cms.CMSApi;
import ch.insign.playauth.PlayAuthApi;
import ch.insign.playauth.party.Party;
import ch.insign.playauth.party.PartyRepository;
import party.User;
import play.data.validation.Constraints;

import javax.inject.Inject;
import javax.validation.Constraint;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

public class UserFormConstraints {

    // --- UniqueEmail

    /**
     * Defines a field as unique email. Collision is allowed only if this email was assigned to currently logged user.
     */
    @Target({FIELD, ANNOTATION_TYPE})
    @Retention(RUNTIME)
    @Constraint(validatedBy = UniqueEmailValidator.class)
    public @interface UniqueEmail {
        String message() default "error.user.email_exists";
        Class<?>[] groups() default {};
        Class<? extends Payload>[] payload() default {};
    }

    /**
     * Validator for {@code @UniqueEmail}
     */
    public static class UniqueEmailValidator implements Constraints.PlayConstraintValidator<UniqueEmail, String> {

        private final PartyRepository<User> partyRepository;
        private final PlayAuthApi playAuthApi;

        @Inject
        public UniqueEmailValidator(PartyRepository partyRepository, PlayAuthApi playAuthApi) {
            this.partyRepository = partyRepository;
            this.playAuthApi = playAuthApi;
        }

        @Override
        public void initialize(UniqueEmail constraintAnnotation) { }

        @Override
        public boolean isValid(String email, ConstraintValidatorContext context) {
            if (email == null) {
                return false;
            }

            return partyRepository.findOneByEmail(email)
                    .map(Party::getId)
                    .map(id -> playAuthApi.getCurrentParty()
                            .map(Party::getId)
                            .filter(id::equals)
                            .isPresent())
                    .orElse(true);
        }
    }

    // --- Language

    /**
     * Defines a range of available languages.
     */
    @Target({FIELD, ANNOTATION_TYPE})
    @Retention(RUNTIME)
    @Constraint(validatedBy = LanguageValidator.class)
    public @interface Language {
        String message() default "user.register.language.wrong";
        Class<?>[] groups() default {};
        Class<? extends Payload>[] payload() default {};
    }

    /**
     * Validator for {@code @Language}
     */
    public static class LanguageValidator implements Constraints.PlayConstraintValidator<Language, String> {

        private final CMSApi cmsApi;

        @Inject
        public LanguageValidator(CMSApi cmsApi) {
            this.cmsApi = cmsApi;
        }

        @Override
        public void initialize(Language constraintAnnotation) { }

        @Override
        public boolean isValid(String language, ConstraintValidatorContext context) {
            return language != null
                    && cmsApi.getConfig().frontendLanguages().stream().anyMatch(language::equals);
        }
    }

    // --- CurrentPassword

    /**
     * Defines that field contains password that matches with the one stored in database for current user.
     */
    @Target({FIELD, ANNOTATION_TYPE})
    @Retention(RUNTIME)
    @Constraint(validatedBy = CurrentPasswordValidator.class)
    public @interface CurrentPassword {
        String message() default "account.dashboard.editProfile.password.wrong";
        Class<?>[] groups() default {};
        Class<? extends Payload>[] payload() default {};
    }

    /**
     * Validator for {@code @CurrentPassword}
     */
    public static class CurrentPasswordValidator implements Constraints.PlayConstraintValidator<CurrentPassword, String> {

        private final PlayAuthApi playAuthApi;

        @Inject
        public CurrentPasswordValidator(PlayAuthApi playAuthApi) {
            this.playAuthApi = playAuthApi;
        }

        @Override
        public void initialize(CurrentPassword constraintAnnotation) { }

        @Override
        public boolean isValid(String password, ConstraintValidatorContext context) {
            if (password == null) {
                return false;
            }

            return playAuthApi.getCurrentParty()
                    .map(Party::getCredentials)
                    .map(String::valueOf)
                    .map(encrypted -> playAuthApi.getPasswordService().passwordsMatch(password, encrypted))
                    .orElse(false);
        }
    }
}
