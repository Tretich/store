package inject;

import auth.LoggingAuthenticationListener;
import ch.insign.playauth.party.PartyRepository;
import ch.insign.playauth.party.PartyService;
import com.google.inject.AbstractModule;
import party.UserRepository;
import party.UserService;

public class DemoProjectAuthModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(PartyRepository.class).to(UserRepository.class);
        bind(PartyService.class).to(UserService.class);
        bind(LoggingAuthenticationListener.class).asEagerSingleton();
    }
}
