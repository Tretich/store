package controller;

import ch.insign.cms.CMSApi;
import ch.insign.cms.controllers.GlobalActionWrapper;
import ch.insign.cms.email.EmailService;
import ch.insign.cms.models.StandalonePage;
import ch.insign.commons.i18n.Language;
import ch.insign.playauth.PlayAuthApi;
import ch.insign.playauth.controllers.actions.RequiresUser;
import ch.insign.playauth.party.Party;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Provider;
import data.form.EditPasswordForm;
import data.form.UserProfileForm;
import data.mapper.UserProfileMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import party.User;
import party.UserRepository;
import party.UserService;
import play.Application;
import play.data.Form;
import play.data.FormFactory;
import play.db.jpa.Transactional;
import play.i18n.MessagesApi;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.With;

import javax.inject.Inject;
import java.io.File;
import java.util.HashMap;
import java.util.Optional;

import static util.DemoProjectBootstrapper.EMAIL_KEY_CHANGE_EMAIL;

@With(GlobalActionWrapper.class)
@RequiresUser
@Transactional
public class AccountController extends Controller {
    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);

    private final PlayAuthApi playAuthApi;
    private final CMSApi cmsApi;
    private final MessagesApi messagesApi;
    private final UserRepository userRepository;
    private final UserService userService;
    private final FormFactory formFactory;
    private final UserProfileMapper userProfileMapper;
    private final EmailService emailService;
    private final Provider<Application> application;
    private final Provider<StandalonePage.Builder> standalonePageBuilder;

    @Inject
    public AccountController(
            PlayAuthApi playAuthApi,
            CMSApi cmsApi,
            UserProfileMapper userProfileMapper,
            UserRepository userRepository,
            MessagesApi messagesApi,
            FormFactory formFactory,
            EmailService emailService,
            Provider<Application> application,
            UserService userService,
            Provider<StandalonePage.Builder> standalonePageBuilder
    ) {
        this.playAuthApi = playAuthApi;
        this.cmsApi = cmsApi;
        this.userRepository = userRepository;
        this.messagesApi = messagesApi;
        this.userProfileMapper = userProfileMapper;
        this.formFactory = formFactory;
        this.emailService = emailService;
        this.application = application;
        this.userService = userService;
        this.standalonePageBuilder = standalonePageBuilder;
    }

    public Result dashboard() {
        return ok(cmsApi.getFilterManager().processOutput(
                views.html.account.dashboard.render(
                        standalonePageBuilder.get()
                                .withTitleKey("account.dashboard")
                                .build(),
                        (User) playAuthApi.getCurrentParty().get()),
                null));
    }

    public Result editProfile() {
        User user = (User) playAuthApi.getCurrentParty().get();

        return ok(cmsApi.getFilterManager().processOutput(
                views.html.account.editProfile.render(
                        standalonePageBuilder.get()
                                .withTitleKey("account.dashboard.editProfile")
                                .build(),
                        formFactory.form(UserProfileForm.class).fill(userProfileMapper.toForm(user)),
                        user),
                null));
    }

    public Result doEditProfile() {
        User user = (User) playAuthApi.getCurrentParty().get();
        Form<UserProfileForm> form = formFactory.form(UserProfileForm.class).bindFromRequest();

        if (form.hasErrors()) {
            return badRequest(cmsApi.getFilterManager().processOutput(
                    views.html.account.editProfile.render(
                            standalonePageBuilder.get()
                                    .withTitleKey("account.dashboard.editProfile")
                                    .build(),
                            form,
                            user),
                    null));
        }

        UserProfileForm userForm = form.get();
        String oldEmail = user.getEmail();

        user = userProfileMapper.update(userForm, user);
        user.setName((user.getFirstName() + " " + user.getLastName()).trim());
        userRepository.save(user);

        if (!user.getEmail().equals(oldEmail)){
            sendChangeEmailEmails(user, oldEmail);
        }

        flash("success-disappear", messagesApi.get(lang(), "account.dashboard.editProfile.success"));
        return ok(cmsApi.getFilterManager().processOutput(
                views.html.account.editProfile.render(
                        standalonePageBuilder.get()
                                .withTitleKey("account.dashboard.editProfile")
                                .build(),
                        form,
                        user),
                null));
    }

    public Result editPassword() {
        User user = (User) playAuthApi.getCurrentParty().get();
        return ok(cmsApi.getFilterManager().processOutput(
                views.html.account.editPassword.render(
                        standalonePageBuilder.get()
                                .withTitleKey("account.dashboard.editPassword")
                                .build(),
                        formFactory.form(EditPasswordForm.class),
                        user),
                null));
    }

    public Result doEditPassword() {
        User user = (User) playAuthApi.getCurrentParty().get();
        Form<EditPasswordForm> form = formFactory.form(EditPasswordForm.class).bindFromRequest();

        if (form.hasErrors()) {
            return badRequest(cmsApi.getFilterManager().processOutput(
                    views.html.account.editPassword.render(
                            standalonePageBuilder.get()
                                    .withTitleKey("account.dashboard.editPassword")
                                    .build(),
                            form,
                            user),
                    null));
        }

        user.setCredentials(playAuthApi.getPasswordService().encryptPassword(form.get().getPassword()));
        sendChangePasswordConfirmationEmail(user);
        flash("success-disappear", messagesApi.get(lang(), "account.dashboard.editPassword.success"));
        return redirect(controller.routes.AccountController.dashboard());
    }

    public Result uploadUserAvatar() {
            ObjectNode result = Json.newObject();
            Optional<Party> maybeUser = playAuthApi.getCurrentParty();
            Http.MultipartFormData body = request().body().asMultipartFormData();
            Http.MultipartFormData.FilePart picture = body.getFile("file");
            if (maybeUser.isPresent()) {
                User user =(User) maybeUser.get();
                userService.deletePreviousAvatarImage(user);
                user.setImage(null);
                if (picture != null && picture.getContentType().toLowerCase().startsWith("image")) {
                    File tmpFile = (File) picture.getFile();
                    try {
                        userService.saveFileWithUniqueAvatarName(user, tmpFile);
                    } catch (Exception e) {
                        logger.error("error: " + e.getMessage());
                        result.put("status", "error");
                        return internalServerError(result);
                    }
                    result.put("status", "ok");
                    result.put("filename", cmsApi.getConfig().imageUploadWWWRoot() + File.separator + user.getId());
                    return ok(result);
                } else {
                    result.put("status", "error");
                    return badRequest(result);
                }
            } else {
                result.put("status", "error");
                return forbidden(result);
            }
    }

    public Result deleteAvatarImage() {
        Optional<Party> maybeUser = playAuthApi.getCurrentParty();
        if(maybeUser.isPresent()){
            try {
                User user = (User) maybeUser.get();
                userService.deletePreviousAvatarImage(user);
                user.setImage(null);
                flash("success-disappear", messagesApi.get(lang(), "account.dashboard.delete.image.success"));
                return redirect(controller.routes.AccountController.editProfile().url());
            } catch (Exception e) {
                    e.printStackTrace();
                    logger.debug("error: " + e.getMessage());
                    return internalServerError("error");
            }

        } else {
            return forbidden("error");
        }
    }

    private void sendChangePasswordConfirmationEmail(User user) {

        HashMap<String, String> emailData = new HashMap<>();
        emailData.put("name", user.getFirstName() + " " + user.getLastName());
        emailData.put("email", user.getEmail());

        emailService.send(
                "password.recovery.success",
                user.getEmail(),
                emailData,
                Language.getCurrentLanguage()
        );
    }

    /**
     * Send emails with info message when user change email
     */
    private void sendChangeEmailEmails (User user, String oldEmail) {

        HashMap<String, String> emailData = new HashMap<>();
        emailData.put("firstname", user.getFirstName());
        emailData.put("lastname", user.getLastName());
        emailData.put("oldEmail", oldEmail);
        emailData.put("newEmail", user.getEmail());
        // send to old email
        emailService.send(
                EMAIL_KEY_CHANGE_EMAIL,
                user.getEmail(),
                emailData,
                Language.getCurrentLanguage()
        );
        // send to new email
        emailService.send(
                EMAIL_KEY_CHANGE_EMAIL,
                oldEmail,
                emailData,
                Language.getCurrentLanguage()
        );
    }
}
