package controller;

import ch.insign.cms.CMSApi;
import ch.insign.cms.controllers.GlobalActionWrapper;
import ch.insign.cms.email.EmailService;
import ch.insign.cms.forms.MyIdentity;
import ch.insign.cms.models.StandalonePage;
import ch.insign.cms.models.party.PartyEvents;
import ch.insign.cms.utils.AjaxResult;
import ch.insign.cms.utils.Error;
import ch.insign.commons.i18n.Language;
import ch.insign.playauth.PlayAuthApi;
import ch.insign.playauth.authz.AccessControlManager;
import ch.insign.playauth.party.PartyRoleManager;
import ch.insign.playauth.party.support.DefaultPartyRole;
import ch.insign.playauth.permissions.PartyPermission;
import com.google.inject.Provider;
import data.form.RegisterUserForm;
import data.form.ResetPasswordForm;
import data.mapper.UserMapper;
import data.model.TokenAction;
import org.apache.commons.lang3.StringUtils;
import party.User;
import party.UserRepository;
import play.data.Form;
import play.data.FormFactory;
import play.db.jpa.JPAApi;
import play.db.jpa.Transactional;
import play.i18n.MessagesApi;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import javax.inject.Inject;
import java.util.HashMap;
import java.util.Optional;
import java.util.UUID;

import static util.DemoProjectBootstrapper.EMAIL_KEY_PASSWORD_RESET;
import static util.DemoProjectBootstrapper.EMAIL_KEY_REGISTRATION_WELCOME;

@Transactional
@With(GlobalActionWrapper.class)
public class UserController extends Controller {

    private final CMSApi cmsApi;
    private final PlayAuthApi playAuthApi;
    private final PartyRoleManager partyRoleManager;
    private final UserRepository userRepository;
    private final PartyEvents partyEvents;
    private final MessagesApi messagesApi;
    private final FormFactory formFactory;
    private final AccessControlManager acm;
    private final EmailService emailService;
    private final JPAApi jpaApi;
    private final UserMapper userMapper;
    private final Provider<StandalonePage.Builder> standalonePageBuilder;

    @Inject
    public UserController(
            CMSApi cmsApi,
            PlayAuthApi playAuthApi,
            PartyRoleManager partyRoleManager,
            UserRepository userRepository,
            MessagesApi messagesApi,
            FormFactory formFactory,
            PartyEvents partyEvents,
            JPAApi jpaApi,
            AccessControlManager acm,
            UserMapper userMapper,
            EmailService emailService,
            Provider<StandalonePage.Builder> standalonePageBuilder) {
        this.cmsApi = cmsApi;
        this.playAuthApi = playAuthApi;
        this.partyRoleManager = partyRoleManager;
        this.userRepository = userRepository;
        this.messagesApi = messagesApi;
        this.formFactory = formFactory;
        this.partyEvents = partyEvents;
        this.acm = acm;
        this.emailService = emailService;
        this.userMapper = userMapper;
        this.jpaApi = jpaApi;
        this.standalonePageBuilder = standalonePageBuilder;
    }

    public Result showRegister(String email) {
        RegisterUserForm newUserForm = new RegisterUserForm();
        if (StringUtils.isNotBlank(email)) {
            newUserForm.setEmail(email);
        }
        Form<RegisterUserForm> registerForm = formFactory.form(RegisterUserForm.class)
                .fill(newUserForm);

        return ok(cmsApi.getFilterManager().processOutput(
                views.html.user.register.render(
                        standalonePageBuilder.get().withTitleKey("register.title").build(),
                        registerForm), null));
    }

    public Result doRegister() {
        Form<RegisterUserForm> form = formFactory.form(RegisterUserForm.class).bindFromRequest();

        if (form.hasErrors()) {
            return badRequest(cmsApi.getFilterManager().processOutput(
                    views.html.user.register.render(
                            standalonePageBuilder.get().withTitleKey("register.title").build(),
                            form), null));
        }


        RegisterUserForm userForm = form.get();
        User user = userMapper.fromForm(userForm);
        user.setName((user.getFirstName() + " " + user.getLastName()).trim());
        user = userRepository.save(user);
        user.addRole(partyRoleManager.findOneByName(DefaultPartyRole.ROLE_USER));
        user.setCredentials(playAuthApi.getPasswordService().encryptPassword(form.get().getPassword()));

        playAuthApi.authenticate(user);

        // Add permissions for the created user
        acm.allowPermission(user, PartyPermission.READ, user);
        acm.allowPermission(user, PartyPermission.EDIT, user);
        acm.allowPermission(user, PartyPermission.REQUEST_PASSWORD_RESET, user);
        acm.allowPermission(user, PartyPermission.EDIT_PASSWORD, user);
        sendWelcomeEmail(user);

        flash("success-disappear", messagesApi.get(lang(), "user.register.success"));
        return redirect(controller.routes.AccountController.dashboard().url());
    }

    public Result sendResetPasswordConfirmation() {
        final Form<MyIdentity> form = formFactory.form(MyIdentity.class).bindFromRequest();

        if (form.hasErrors()) {
            return AjaxResult.error(form);
        }

        userRepository.findOneByEmail(form.get().getEmail())
                .ifPresent(this::sendEmailRestorePassword);

        flash("success-disappear", messagesApi.get(lang(), "reset.password.msg.reset_email_sent"));
        return redirect(controller.routes.UserController.showResetPasswordPage());
    }

    public Result showResetPasswordPage() {
        Form<MyIdentity> form = formFactory.form(MyIdentity.class);
        return ok(cmsApi.getFilterManager().processOutput(
                views.html.user.resetPassword.render(
                        standalonePageBuilder.get().withTitleKey("reset.password.title").build(),
                        form),
                null));
    }

    public Result showResetPasswordConfirmationPage(String token, String email) {
        Optional<TokenAction> maybeTokenAction = jpaApi.em()
                .createNamedQuery("TokenAction.findByToken", TokenAction.class)
                .setParameter("token", token)
                .getResultList()
                .stream()
                .findFirst();

        if (!maybeTokenAction.isPresent()) {
            return Error.notFound(messagesApi.get(lang(), "reset.password.msg.token_not_exists"));
        }

        TokenAction tokenAction = maybeTokenAction.get();

        if (tokenAction.isExpired()) {
            return Error.notFound(messagesApi.get(lang(), "reset.password.msg.token_expired"));
        }

        User user = tokenAction.getTargetUser();

        if (!StringUtils.equals(email, user.getEmail())) {
            return Error.notFound(messagesApi.get(lang(), "reset.password.msg.invalid_email"));
        }

        ResetPasswordForm formData = new ResetPasswordForm();
        formData.setToken(tokenAction.getToken());

        Form<ResetPasswordForm> resetPasswordForm = formFactory
                .form(ResetPasswordForm.class)
                .fill(formData);

        return ok(cmsApi.getFilterManager().processOutput(
                views.html.user.resetPasswordConfirmation.render(
                        standalonePageBuilder.get().withTitleKey("reset.password.title").build(),
                        resetPasswordForm),
                null));
    }

    public Result doResetPassword() {
        final Form<ResetPasswordForm> resetPasswordForm = formFactory
                .form(ResetPasswordForm.class)
                .bindFromRequest();

        if (resetPasswordForm.hasErrors()) {
            return badRequest(cmsApi.getFilterManager().processOutput(
                    views.html.user.resetPasswordConfirmation.render(
                            standalonePageBuilder.get().withTitleKey("reset.password.title").build(),
                            resetPasswordForm),
                    null));
        }

        ResetPasswordForm formData = resetPasswordForm.get();

        Optional<TokenAction> maybeTokenAction = jpaApi.em()
                .createNamedQuery("TokenAction.findByToken", TokenAction.class)
                .setParameter("token", formData.getToken())
                .getResultList()
                .stream()
                .findFirst();

        if (!maybeTokenAction.isPresent()) {
            return Error.notFound(messagesApi.get(lang(), "reset.password.msg.token_not_exists"));
        }

        TokenAction tokenAction = maybeTokenAction.get();

        if (tokenAction.isExpired()) {
            return Error.notFound(messagesApi.get(lang(), "reset.password.msg.token_expired"));
        }

        User user = tokenAction.getTargetUser();
        user.setCredentials(playAuthApi.getPasswordService().encryptPassword(formData.getPassword()));
        user = jpaApi.em().merge(user);

        partyEvents.onPasswordUpdate(resetPasswordForm, user);

        jpaApi.em().remove(tokenAction);

        playAuthApi.authenticate(user);

        flash("success-disappear", messagesApi.get(lang(), "reset.password.msg.success"));
        return redirect(controller.routes.AccountController.dashboard());
    }

    /**
     * Send email with link to reset password
     */
    private void sendEmailRestorePassword(User user) {
        final String token = UUID.randomUUID().toString();
        TokenAction ta = TokenAction.create(token, user, user.getEmail());
        jpaApi.em().persist(ta);

        HashMap<String, String> emailData = new HashMap<>();
        emailData.put("firstname", user.getFirstName());
        emailData.put("lastname", user.getLastName());
        emailData.put("url", controller.routes.UserController.showResetPasswordConfirmationPage(token, user.getEmail())
                .absoluteURL(request(), true));

        emailService.send(
                EMAIL_KEY_PASSWORD_RESET,
                user.getEmail(),
                emailData,
                Language.getCurrentLanguage()
        );
    }

    /**
     * Send email with welcome message after registration
     */
    private void sendWelcomeEmail(User user) {

        HashMap<String, String> emailData = new HashMap<>();
        emailData.put("firstname", user.getFirstName());
        emailData.put("lastname", user.getLastName());
        emailData.put("email", user.getEmail());

        emailService.send(
                EMAIL_KEY_REGISTRATION_WELCOME,
                user.getEmail(),
                emailData,
                Language.getCurrentLanguage()
        );
    }
}
