package service;

import ch.insign.cms.services.AbstractDatatableService;
import crud.controller.routes;
import crud.data.entity.Product;
import org.apache.commons.lang3.StringUtils;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.persistence.Query;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class ProductDatableService extends AbstractDatatableService<Product> {

    private final String[] SORTABLE_FIELDS = {"id", "name", "description", "price", "category"};
    private final String[] SEARCHABLE_FIELDS = {"name", "description", "price"};

    private final JPAApi jpaApi;

    @Inject
    public ProductDatableService(JPAApi jpaApi) {
        super(jpaApi);
        this.jpaApi = jpaApi;
    }

    @Override
    protected CriteriaQuery buildSearchCriteria(
            CriteriaQuery query,
            CriteriaBuilder builder,
            Root root,
            String keywords,
            String orderByField,
            String orderByDirection) {
        Root<Product> products = (Root<Product>) root;

        List<Predicate> conditionsAnd = new ArrayList<>();
        List<String> searchFields = Arrays.asList(SEARCHABLE_FIELDS);

        for (String keyword : StringUtils.split(keywords)) {
            Predicate[] likeConditions = searchFields.stream()
                    .map(field -> builder.like(builder.lower(products.get(field)), "%" + keyword.trim().toLowerCase() + "%"))
                    .toArray(Predicate[]::new);

            conditionsAnd.add(builder.or(likeConditions));
        }

        query.where(builder.and(conditionsAnd.toArray(new Predicate[]{})));

        if (StringUtils.isNotEmpty(orderByField)) {
            Expression orderByFieldExpression = products.get(orderByField);

            query.orderBy(StringUtils.defaultIfEmpty(orderByDirection, "desc").equals("desc")
                    ? builder.desc(orderByFieldExpression)
                    : builder.asc(orderByFieldExpression));
        }

        return query;
    }

    @Override
    protected long getTotalSize() {
        Query query = jpaApi.em().createQuery("select count (p) from Product p", Long.class);
        long count = (Long) query.getSingleResult();
        return count;
    }

    @Override
    protected List getSortableFields() {
        return Arrays.asList(SORTABLE_FIELDS);
    }

    @Override
    protected List<Function<Product, String>> getTableFields() {
        return Arrays.asList(
                product -> String.valueOf(product.getId()),
                product -> product.getName(),
                product -> product.getDescription(),
                product -> String.valueOf(product.getPrice()),
                product -> product.getCategory().getName() ,
                product -> {
                    String data ="";
                    //if (playAuthApi.isPermitted(productPermission.EDIT, product)) {
                        data = "<a title=\"edit\" href=\"" +
                                routes.ProductController.edit(product.getId()).url()
                                + "\"><i class=\"fa fa-edit\"></i></a> &nbsp;";
                    //}
                    //if (playAuthApi.isPermitted(productPermission.DELETE, product)) {
                        data = data + "<a class=\"delete-product\" data-url=\"" +
                                routes.ProductController.deleteRequest(product.getId()).url() +
                                "\" href=\"" +
                                routes.ProductController.deleteRequest(product.getId()).url() +
                                "\"><i class=\"fa fa-trash\"></i></a> &nbsp;";
                    //}
                    if(StringUtils.isBlank(data)) {
                        data = "-";
                    }
                    return data;
                }
        );
    }
}
