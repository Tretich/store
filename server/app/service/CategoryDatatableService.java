package service;

import ch.insign.cms.services.AbstractDatatableService;
import crud.controller.routes;
import crud.data.entity.Product;
import crud.data.entity.categories.Category;
import org.apache.commons.lang3.StringUtils;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.persistence.Query;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class CategoryDatatableService extends AbstractDatatableService<Category> {
    private final String[] SORTABLE_FIELDS = {"id", "name"};
    private final String[] SEARCHABLE_FIELDS = {"name"};

    JPAApi jpaApi;

    @Inject
    public CategoryDatatableService(JPAApi jpaApi) {
        super(jpaApi);
        this.jpaApi = jpaApi;
    }

    @Override
    protected <E> CriteriaQuery<E> buildSearchCriteria(CriteriaQuery<E> query, CriteriaBuilder builder, Root<Category> root, String keywords, String orderByField, String orderByDirection) {
        Root<Category> products = (Root<Category>) root;

        List<Predicate> conditionsAnd = new ArrayList<>();
        List<String> searchFields = Arrays.asList(SEARCHABLE_FIELDS);

        for (String keyword : StringUtils.split(keywords)) {
            Predicate[] likeConditions = searchFields.stream()
                    .map(field -> builder.like(builder.lower(products.get(field)), "%" + keyword.trim().toLowerCase() + "%"))
                    .toArray(Predicate[]::new);

            conditionsAnd.add(builder.or(likeConditions));
        }

        query.where(builder.and(conditionsAnd.toArray(new Predicate[]{})));

        if (StringUtils.isNotEmpty(orderByField)) {
            Expression orderByFieldExpression = products.get(orderByField);

            query.orderBy(StringUtils.defaultIfEmpty(orderByDirection, "desc").equals("desc")
                    ? builder.desc(orderByFieldExpression)
                    : builder.asc(orderByFieldExpression));
        }

        return query;
    }

    @Override
    protected long getTotalSize() {
        Query query = jpaApi.em().createQuery("select count (c) from Category c", Long.class);
        Long size = (Long) query.getSingleResult();

        return size;
    }

    @Override
    protected List<String> getSortableFields() {
        return (Arrays.asList(SORTABLE_FIELDS));
    }

    @Override
    protected List<Function<Category, String>> getTableFields() {
        return Arrays.asList(
                category -> String.valueOf(category.getId()),
                category -> category.getName(),
                category -> {
                    String data ="";
                    //if (playAuthApi.isPermitted(productPermission.EDIT, product)) {
                    data = "<a title=\"edit\" href=\"" +
                            routes.CategoryController.edit(category.getId()).url()
                            + "\"><i class=\"fa fa-edit\"></i></a> &nbsp;";
                    //}
                    //if (playAuthApi.isPermitted(productPermission.DELETE, product)) {
                    data = data + "<a class=\"delete-category\" data-url=\"" +
                            routes.CategoryController.deleteRequest(category.getId()).url() +
                            "\" href=\"" +
                            routes.ProductController.deleteRequest(category.getId()).url() +
                            "\"><i class=\"fa fa-trash\"></i></a> &nbsp;";
                    //}
                    if(StringUtils.isBlank(data)) {
                        data = "-";
                    }
                    return data;
                }
        );
    }
}
