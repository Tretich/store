package service

import javax.inject._
import party.UserService
import play.db.jpa.JPAApi
import shared.{Api, UserItem}

import java.util.function.Supplier
import scala.collection.JavaConversions._

class ApiService @Inject() (jPAApi: JPAApi, userService: UserService) extends Api {

  // message of the day
  override def simpleAjaxCall(name: String): String = "this is a plain string originating from server"

  // get User items
  override def getUsers(): Seq[UserItem] = {
    jPAApi.withTransaction(new Supplier[Seq[UserItem]]{
      override def get(): Seq[UserItem] = {
        val users = userService.findAll()
        users.map(u => UserItem(u.getId, u.getName, u.getEmail)).toSeq
      }
    })
  }

}
