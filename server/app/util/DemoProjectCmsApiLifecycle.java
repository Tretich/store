package util;

import auth.LoggingAuthenticationListener;
import ch.insign.cms.CMSApi;
import ch.insign.cms.CMSApiLifecycleImpl;
import ch.insign.cms.blocks.jotformpageblock.service.JotFormService;
import ch.insign.playauth.PlayAuthApi;
import ch.insign.playauth.party.PartyRepository;
import play.db.jpa.JPAApi;
import widgets.registeredusers.RegisteredUsersWidget;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class DemoProjectCmsApiLifecycle extends CMSApiLifecycleImpl {

    private final PlayAuthApi playAuthApi;
    private final PartyRepository partyRepository;
    private final LoggingAuthenticationListener loggingAuthenticationListener;

    @Inject
    public DemoProjectCmsApiLifecycle(
            JPAApi jpaApi,
            CMSApi cmsApi,
            PlayAuthApi playAuthApi,
            JotFormService jotFormService,
            PartyRepository partyRepository,
            LoggingAuthenticationListener loggingAuthenticationListener
    ) {
        super(cmsApi, jpaApi, jotFormService);
        this.playAuthApi = playAuthApi;
        this.partyRepository = partyRepository;
        this.loggingAuthenticationListener = loggingAuthenticationListener;
    }

    @Override
    public void onStart() {
        super.onStart();
        playAuthApi.getEventDispatcher().addSubscriber(loggingAuthenticationListener);
    }

    @Override
    protected void registerContentFilters(CMSApi cmsApi) {
        super.registerContentFilters(cmsApi);
        cmsApi.getFilterManager().register(new RegisteredUsersWidget(partyRepository));
    }

    @Override
    protected void registerUncachedPartials(CMSApi cmsApi) {
        super.registerUncachedPartials(cmsApi);

        cmsApi.getUncachedManager()
                .register("flashPartial", () -> ch.insign.cms.views.html.admin.shared.flashPartial.render());
    }

}
