package util;

import blocks.errorblock.DefaultErrorPage;
import blocks.pageblock.DefaultPage;
import blocks.teaserblock.TeaserBlock;
import ch.insign.cms.CMSApi;
import ch.insign.cms.CMSTemplateApi;
import ch.insign.cms.blocks.backendlinkblock.BackendLinkBlock;
import ch.insign.cms.blocks.errorblock.ErrorPage;
import ch.insign.cms.blocks.groupingblock.GroupingBlock;
import ch.insign.cms.blocks.horizontalcollection.HorizontalCollectionBlock;
import ch.insign.cms.models.*;
import ch.insign.cms.permissions.ApplicationPermission;
import ch.insign.cms.permissions.BlockPermission;
import ch.insign.commons.i18n.Language;
import ch.insign.playauth.authz.AccessControlManager;
import ch.insign.playauth.party.*;
import ch.insign.playauth.party.support.DefaultPartyRole;
import com.typesafe.config.Config;
import crud.page.CarInventoryPage;
import crud.data.entity.Brand;
import crud.data.entity.Car;
import crud.data.repository.BrandRepository;
import crud.data.repository.CarRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import party.User;
import party.UserService;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Date;

import static ch.insign.playauth.party.support.DefaultPartyRole.ROLE_USER;

public class DemoProjectBootstrapper extends DefaultBootstrapper {
    public static final String EMAIL_KEY_PASSWORD_RESET = "password.reset";
    public static final String EMAIL_KEY_REGISTRATION_WELCOME = "user.registration.welcome";
    public static final String EMAIL_KEY_CHANGE_EMAIL = "user.account.change.email";

    private static final Logger logger = LoggerFactory.getLogger(DemoProjectBootstrapper.class);

    // Predefined Demo Role
    private static final String ROLE_DEMO_ROLE = "DemoRole";

    private final CMSTemplateApi cmsTemplateApi;
    private final PartyRoleManager prm;
    private final AccessControlManager acm;
    private final UserService userService;
    private final CarRepository carRepository;
    private final BrandRepository brandRepository;

    @Inject
    public DemoProjectBootstrapper(
            Config config,
            AccessControlManager accessControlManager,
            CMSApi cmsApi,
            CMSTemplateApi cmsTemplateApi,
            PartyRoleManager partyRoleManager,
            JPAApi jpaApi,
            UserService userService,
            CarRepository carRepository,
            BrandRepository brandRepository
    ) {
        super(config, accessControlManager, cmsApi, cmsTemplateApi, partyRoleManager, jpaApi);
        this.prm = partyRoleManager;
        this.acm = accessControlManager;
        this.cmsTemplateApi = cmsTemplateApi;
        this.userService = userService;
        this.carRepository = carRepository;
        this.brandRepository = brandRepository;
    }

    @Override
    protected void deleteExampleData() {
        super.deleteExampleData();
        carRepository.findAll().getAll().forEach(carRepository::delete);
        brandRepository.findAll().getAll().forEach(brandRepository::delete);
    }

    @Override
    public synchronized void loadExampleSiteData(Sites.Site site) {
        super.loadExampleSiteData(site);

        createTeaserBlocks(site);
        createWidgetExampleBlock();
        createErrorTemplates(site);
        createWelcomeTemplate(site);
        createChangeEmailTemplate(site);
        createCrudExampleData();
        createCarInventoryBlock();
    }

    @Override
    protected void deleteEssentialData() {
        super.deleteEssentialData();
        EmailTemplate.find.all().forEach(emailTemplate -> emailTemplate.delete());
    }

    @Override
    public synchronized void loadEssentialSiteData(Sites.Site site) {
        super.loadEssentialSiteData(site);

        EmailTemplate template = new EmailTemplate();
        template.setTemplateKey(EMAIL_KEY_PASSWORD_RESET);
        template.setSite(site.key);
        template.setSender("admin@localhost");
        template.setCategory(EmailTemplate.EmailTemplateCategory.EXTERN);
        template.getContent().set("en", "<p>Click next link in order to change your password</p>" +
                "<p><a href=\"{url}\">{url}</a></p> ");
        template.getContent().set("de", "<p>Click next link in order to change your password</p>" +
                "<p><a href=\"{url}\">{url}</a></p>  ");
        template.getDescription().set("en", "Password reset requested.");
        template.getDescription().set("en", "Password reset requested.");
        template.getSubject().set("en", "Password reset requested.");
        template.getSubject().set("de", "Password reset requested.");
        template.save();
    }

    @Override
    protected void createBackendNavigation() {
        super.createBackendNavigation();

        GroupingBlock root = GroupingBlock.find.byKey(PageBlock.KEY_BACKEND);

        // Adding our example apps to the backend

        String lang = Language.getCurrentLanguage();
        String key = "_backend_myapp";

        if (BackendLinkBlock.find.byKey(key) == null) {
            BackendLinkBlock page = new BackendLinkBlock();
            root.getSubBlocks().add(page);
            page.setParentBlock(root);
            page.setKey(key);
            page.getLinkTarget().set(lang, "/admin/myapp");
            page.getNavTitle().set("en", "My app");
            page.getNavTitle().set("de", "Meine app");
            page.setLinkIcon("fa-link");
            page.save();
        }

        String crud_page_key = "_backend_crud_example";

        if (BackendLinkBlock.find.byKey(crud_page_key) == null) {
            BackendLinkBlock page = new BackendLinkBlock();
            root.getSubBlocks().add(page);
            page.setParentBlock(root);
            page.setKey(crud_page_key);
            page.getLinkTarget().set(lang, crud.controller.routes.CarController.list().url());
            page.getNavTitle().set("en", "CRUD Example");
            page.getNavTitle().set("de", "CRUD Beispiel");
            page.setLinkIcon("fa-link");
            page.save();
        }
    }

    @Override
    protected void createPartyRoles() {
        super.createPartyRoles();

        PartyRole demoRole = prm.create(ROLE_DEMO_ROLE);

        // Add default permissions to new DemoRole
        acm.allowPermission(demoRole, ApplicationPermission.BROWSE_BACKEND);
        acm.allowPermission(demoRole, BlockPermission.MODIFY);
    }

    @Override
    protected void createParties() {
        super.createParties();

        PartyRole superuserRole = prm.findOneByName(DefaultPartyRole.ROLE_SUPERUSER);
        PartyRole demoRole = prm.findOneByName(ROLE_DEMO_ROLE);
        PartyRole userRole = prm.findOneByName(ROLE_USER);

        if (!userService.findOneByEmail("admin@insign.ch").isPresent()) {
            User superuser = new User();
            superuser.setName("admin");
            superuser.setCredentials("temp123");
            superuser.setEmail("admin@insign.ch");
            superuser.setFirstName("admin");
            superuser.setLastName("insign");
            superuser.setGender(ISOGender.MALE);
            superuser = userService.save(superuser);
            superuser.addRole(superuserRole);
        }

        if (!userService.findOneByEmail("demouser@insign.ch").isPresent()) {
            User demouser = new User();
            demouser.setName("demouser");
            demouser.setCredentials("temp123");
            demouser.setEmail("demouser@insign.ch");
            demouser.setFirstName("demouser");
            demouser.setLastName("insign");
            demouser.setGender(ISOGender.MALE);
            demouser = userService.save(demouser);
            demouser.addRole(demoRole);
            demouser.addRole(userRole);
        }
    }

    private void createWidgetExampleBlock() {
        logger.info("Creating widget example block");

        try {
            AbstractBlock homepage = AbstractBlock.find.byKey(PageBlock.KEY_HOMEPAGE);
            CollectionBlock sidebar = cmsTemplateApi.addBlockToSlot(CollectionBlock.class, homepage, "sidebar");
            ContentBlock contentBlock = sidebar.addSubBlock(ContentBlock.class);
            contentBlock.getTitle().set("en", "Widgets");
            contentBlock.getContent().set("en",
                    "<p>Content filters are a great tool to let the user add variables freely inside any content, " +
                            "e.g. [[currentUserCount]] which your filter resolves to the current value " +
                            "when displaying the page.</p>\n" +
                            "<h4>Content filter example.</h4>\n" +
                            "<p>Last registered users: [[registeredUsersWidget:5]]</p>\n" +
                            "<p><a href=\"https://confluence.insign.ch/display/PLAY/Play+CMS\">" +
                            "Learn more </a> about filter framework</p>");
            contentBlock.save();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void createTeaserBlocks(Sites.Site site) {
        logger.info("Creating teaser blocks");

        try {
            AbstractBlock homepage = AbstractBlock.find.byKey(PageBlock.KEY_HOMEPAGE, site.key);
            CollectionBlock bottomPane = cmsTemplateApi.addBlockToSlot(HorizontalCollectionBlock.class, homepage, "bottom");
            TeaserBlock block, block2, block3;
            block = bottomPane.addSubBlock(TeaserBlock.class);
            block.getTitle().set("en", "Teaser block");
            block.getSubtitle().set("en", "Lorep Ipsum");
            block.getContent().set("en", "This is an example of customizing cms content blocks");
            block.getLogoUrl().set("en", "/assets/images/yacht.png");
            block.getLinkUrl().set("en", "confluence.insign.ch/display/PLAY/Create+your+own+cms+block");
            block.getLinkText().set("en", "Learn more");
            block.setSite(site.key);

            block2 = bottomPane.addSubBlock(TeaserBlock.class);
            block2.getTitle().set("en", "Teaser block");
            block2.getSubtitle().set("en", "Lorep Ipsum");
            block2.getContent().set("en", "This is an example of customizing cms content blocks");
            block2.getLogoUrl().set("en", "/assets/images/yacht2.jpg");
            block2.getLinkUrl().set("en", "confluence.insign.ch/display/PLAY/Create+your+own+cms+block");
            block2.getLinkText().set("en", "Learn more");
            block2.setSite(site.key);

            block3 = bottomPane.addSubBlock(TeaserBlock.class);
            block3.getTitle().set("en", "Teaser block");
            block3.getSubtitle().set("en", "Lorep Ipsum");
            block3.getContent().set("en", "This is an example of customizing cms content blocks");
            block3.getLogoUrl().set("en", "/assets/images/yacht3.jpg");
            block3.getLinkUrl().set("en", "confluence.insign.ch/display/PLAY/Create+your+own+cms+block");
            block3.getLinkText().set("en", "Learn more");
            block3.setSite(site.key);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void createCarInventoryBlock() {
        logger.info("Creating car inventory block");

        String frontendBlockKeyForCRUD = "_frontend_crud_example";

        CarInventoryPage page = (CarInventoryPage) CarInventoryPage.find.byKey(frontendBlockKeyForCRUD);
        if (page == null) {
            page = new CarInventoryPage();
            page.setDealerTitle("Your favourite car dealer");
            page.setKey(frontendBlockKeyForCRUD);
            page.getPageTitle().set("en", "Car Inventory");
            page.getPageTitle().set("de", "Car Inventory");
            page.getMetaTitle().set("en", "Car Inventory");
            page.getMetaTitle().set("de", "Car Inventory");
            page.getNavTitle().set("en", "Car Inventory");
            page.getNavTitle().set("de", "Car Inventory");
            GroupingBlock frontendGroupingBlock = GroupingBlock.find.frontend();
            if (frontendGroupingBlock != null) {
                frontendGroupingBlock.getSubBlocks().add(page);
                page.setParentBlock(frontendGroupingBlock);
            }
            page.save();

            createNavItem(page, "/carInventory", "en");
        }
    }

    private void createCrudExampleData() {
        deleteExampleData();
        logger.info("Adding CRUD example data");

        Brand brand1 = new Brand();
        brand1.setTitle("Porsche");
        brandRepository.save(brand1);

        Brand brand2 = new Brand();
        brand2.setTitle("OPEL");
        brandRepository.save(brand2);

        Car car1 = new Car();
        car1.setModel("Carrera");
        car1.setBuyDate(Date.from(Instant.now()));
        car1.setBrand(brand1);
        car1.setPrice(new BigDecimal(15000));
        car1.setRegistrationId("AA 5676 CH");
        carRepository.save(car1);

        Car car2 = new Car();
        car2.setModel("Boxter");
        car2.setBuyDate(Date.from(Instant.now()));
        car2.setBrand(brand1);
        car2.setPrice(new BigDecimal(20000));
        car2.setRegistrationId("M78699");
        carRepository.save(car2);

        Car car3 = new Car();
        car3.setModel("Targa");
        car3.setBrand(brand1);
        car3.setPrice(new BigDecimal(11455));
        car3.setRegistrationId("CD ZH8 38");
        carRepository.save(car3);

        Car car4 = new Car();
        car4.setModel("Kadett");
        car4.setBrand(brand2);
        car4.setPrice(new BigDecimal(10500));
        car4.setRegistrationId("CD Z56461");
        carRepository.save(car4);
    }

    @Override
    protected PageBlock getNewPageInstance() {
        return new DefaultPage();
    }

    @Override
    protected ErrorPage getErrorPageInstance() {
        return new DefaultErrorPage();
    }

    /**
     * Create an EmailTemplate for sending email to user after password reset
     */
    private void createErrorTemplates(Sites.Site site) {
        logger.info("Creating error templates");

        EmailTemplate template = new EmailTemplate();
        template.setTemplateKey("password.recovery.success");
        template.setSender("admin@localhost");
        template.setCategory(EmailTemplate.EmailTemplateCategory.EXTERN);
        template.getContent().set("en", "Hello {name}. Password for your account has been updated. ");
        template.getContent().set("de", "Hello {name}. Password for your account has been updated. ");
        template.getDescription().set("en", "Password has been updated.");
        template.getDescription().set("en", "Password has been updated.");
        template.getSubject().set("en", "Password has been updated.");
        template.getSubject().set("de", "Password has been updated.");
        template.setSite(site.key);

        template.save();
    }

    /**
     * Create an EmailTemplate for sending email to user after registration
     */
    private void createWelcomeTemplate(Sites.Site site) {
        logger.info("Creating welcome email template");

        EmailTemplate template = new EmailTemplate();
        template.setTemplateKey(EMAIL_KEY_REGISTRATION_WELCOME);
        template.setSender("admin@localhost");
        template.setCategory(EmailTemplate.EmailTemplateCategory.EXTERN);
        template.getContent().set("en", "Hello {firstname} {lastname}. New account for this email {email} has been created. ");
        template.getContent().set("de", "Hello {firstname} {lastname}. New account for this email {email} has been created. ");
        template.getDescription().set("en", "Welcome to play-cms-demo");
        template.getDescription().set("en", "Welcome to play-cms-demo.");
        template.getSubject().set("en", "Welcome to play-cms-demo.");
        template.getSubject().set("de", "Welcome to play-cms-demo.");
        template.setSite(site.key);

        template.save();
    }

    /**
     * Create an EmailTemplate for sending email to user after change email
     */
    private void createChangeEmailTemplate(Sites.Site site) {
        logger.info("Creating welcome email template");

        EmailTemplate template = new EmailTemplate();
        template.setTemplateKey(EMAIL_KEY_CHANGE_EMAIL);
        template.setSender("admin@localhost");
        template.setCategory(EmailTemplate.EmailTemplateCategory.EXTERN);
        template.getContent().set("en", "Hello {firstname} {lastname}. Email for your account has been changed. Old email: {oldEmail}. New email: {newEmail}.");
        template.getContent().set("de", "Hello {firstname} {lastname}. Email for your account has been changed. Old email: {oldEmail}. New email: {newEmail}.");
        template.getDescription().set("en", "Email was changed for account on play-cms-demo.");
        template.getDescription().set("en", "Email was changed for account on play-cms-demo.");
        template.getSubject().set("en", "Email was changed for account on play-cms-demo.");
        template.getSubject().set("de", "Email was changed for account on play-cms-demo.");
        template.setSite(site.key);

        template.save();
    }

    private void createNavItem(PageBlock page, String vpathEN, String langCode) {
        NavigationItem navItem = page.createNavItem(langCode);
        navItem.setVisible(true);

        try {
            navItem.setVirtualPath(vpathEN);
        } catch (NavigationItem.VpathNotAvailableException e) {
            logger.error("loadEssentialData(): Vpath not available: " + vpathEN, e);
        } catch (NavigationItem.VpathNotValidException e) {
            logger.error("loadEssentialData(): Vpath not valid: " + vpathEN, e);
        }

        navItem.save();
    }
}
